from flectra import models, fields, api
import logging
from datetime import   datetime
from flectra.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
from dateutil.relativedelta import relativedelta
class AttendanceRecapReportWizard(models.TransientModel):
    _name = 'activtiy.crm.report.wizard'

    user_id = fields.Many2many('res.users',string='Users')
    date_start = fields.Date(string="Start Date", required=True, default=fields.Date.today)
    date_end = fields.Date(string="End Date", required=True, default=fields.Date.to_string(datetime.today() + relativedelta(days=30)) )

    @api.multi
    def get_report(self):
        """Call when button 'Get Report' clicked.
        """
        data = {
            'ids': self.ids,
            'model': self._name,
            'form': {
                'date_start': self.date_start,
                'user_id': self.user_id.ids,
                'date_end': self.date_end,
            },
        }

        # use `module_name.report_id` as reference.
        # `report_action()` will call `_get_report_values()` and pass `data` automatically.
        return self.env.ref('crm_extension.action_crm_activity_report').report_action(self, data=data)


class ReportAttendanceRecap(models.AbstractModel):
    """Abstract Model for report template.
    for `_name` model, please use `report.` as prefix then add `module_name.report_name`.
    """

    _name = 'report.crm_extension.activtiy_by_user_report'

    @api.model
    def get_report_values(self, docids, data=None):
        date_start = data['form']['date_start']
        date_end = data['form']['date_end']
        user_id = data['form']['user_id']
        date_start_obj = datetime.strptime(date_start, DATE_FORMAT)
        date_end_obj = datetime.strptime(date_end, DATE_FORMAT)

       
        user_ids = self.env['res.users'].search([('id','in',user_id )])
       
        user1 = []
        docs = []
        schedule = []
        schedule_history = []
        schedule_activties = self.env['mail.activity'].search([])
        done_activities = self.env['done.activity'].search([])
        schdule_history = self.env['activities.history'].search([])

        for user in user_ids:
            user1.append({
                'name':user.name,
                'function':user.partner_id.function,
                'level':user.partner_id.level.name,
                'country':user.partner_id.country_id.name
            })

            for rec in schedule_activties:
                if user.id in rec.user_ids.ids:
                    if rec.create_date >= date_start_obj.strftime(DATETIME_FORMAT) and rec.create_date <= date_end_obj.strftime(DATETIME_FORMAT):
                        if rec.res_model == 'crm.lead':
                            lead = self.env['crm.lead'].search([('id','=',rec.res_id)])
                            if lead:
                                schedule.append({
                                    'user':user.name,
                                    'sch_name': rec.summary if rec.summary else '' ,
                                    'sch_type': rec.activity_type_id.name if rec.activity_type_id else '',
                                    'sch_user_ids': rec.user_ids if rec.user_ids else '',
                                    'schedule_date':rec.create_date,
                                    'sch_lead':lead,
                                    'sch_date_deadline':rec.date_deadline if rec.date_deadline else '' , 
                                })
            
            



        
            for done_activity in done_activities:
                if user.id in done_activity.user_ids.ids:
                    if done_activity.create_date >= date_start_obj.strftime(DATETIME_FORMAT) and done_activity.create_date <= date_end_obj.strftime(DATETIME_FORMAT):
                        lead = self.env['crm.lead'].search([('id','=',done_activity.res_id)])
                        if lead:
                            docs.append({
                                'done_user':user.name,
                                'done_name': done_activity.name if done_activity.name else '' ,
                                'done_state': done_activity.state if done_activity.state else '',
                                'done_type': done_activity.type if done_activity.type else '',
                                'done_done_by_id':done_activity.done_by_id.name if done_activity.done_by_id.name else '' ,
                                'done_user_ids': done_activity.user_ids if done_activity.user_ids else '',
                                'done_date':done_activity.create_date,
                                'done_date_deadline':done_activity.date_deadline if done_activity.date_deadline else '' ,
                                'done_lead':lead,
                            })
        
       
            for s in schdule_history:
                if user.id in s.user_ids.ids:
                    if s.create_date >= date_start_obj.strftime(DATETIME_FORMAT) and s.create_date <= date_end_obj.strftime(DATETIME_FORMAT):
                        if s.res_model == 'crm.lead':
                            lead = self.env['crm.lead'].search([('id','=',s.res_id)])
                            if lead:
                                schedule_history.append({
                                    'his_user':user.name,
                                    'his_name': s.name if s.name else '' ,
                                    'his_type': s.activity_type_id.name if s.activity_type_id else '',
                                    'his_user_ids': s.user_ids if s.user_ids else '',
                                    'his_date':s.create_date,
                                    'his_date_deadline':s.date_deadline if s.date_deadline else '' ,
                                    'his_lead':lead,
                                })

        
        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'user_id': user1,
            'date_start': date_start,
            'date_end': date_end,
            'sch_activties':schedule,
            'done_activtiy': docs,
            'schedule_history':schedule_history,
        }