
{
    'name' : 'CRM Extension',
    'author': 'ERISP (Pvt) Ltd.',
    'description': """
    Extended CRM view for Nets-international.
    """,
    'depends' : [
        'base',
        'mail',
        'crm',
        'sale',
        'stock',
        'purchase',
        'branch_enhancement',
        'activity_extension',
        
    ],
    'data' :[
        'security/ir.model.access.csv',
        'security/rules.xml',
        'views/crm.xml',
        'views/schedule_activity.xml',
        'reports/activity.xml',
        'reports/activity_by_user.xml',
        'reports/activtiy_report_template.xml',
        
        'data/cv_type.xml',
        'data/bu.xml'
    ],
    'qweb': [
        'static/activity.xml'
    ],
    'version': '1.6.1.0',
}
