from flectra import models, fields, api, _
import logging



class ActivityInherited(models.Model):
    _inherit = "mail.activity"

    def _get_lead(self):
        pass
        # logging.info("##################1121")
        # logging.info(self.res_model)
        # logging.info(type(self.res_model))
        # for activity in self:
        #     if activity.res_model == 'crm.lead':
        #         lead = self.env['crm.lead'].search([('id','=',activity.res_id)],limit=1)
        #         logging.info("----------------------")
        #         logging.info(lead)
        #         return lead
    crm_lead = fields.Many2one("crm.lead","Lead" )
    partner = fields.Many2one('res.partner',"Customer")
    partner_company = fields.Char("Company")
    
    @api.model
    def create(self, vals): 
        res = super(ActivityInherited, self).create(vals) 
        if res.res_model == 'crm.lead':
            lead = self.env['crm.lead'].search([('id','=', res.res_id )],limit=1)
            res.crm_lead = lead.id
            res.partner = lead.partner_id.id
            res.partner_company = lead.partner_name
        return res