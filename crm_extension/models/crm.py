from flectra import models, fields, api, _
import logging



class crm_inherited(models.Model):
    _inherit = "res.partner"
    level = fields.Many2one('crm.level', string="Level")
    company_size = fields.Many2one('crm.company.size', string = "Company Size")
    department = fields.Many2one('crm.department', string = "Department")
    social_media = fields.Char(string="Social Media")
    business_unit = fields.Many2one('crm.business.unit', string="Business Unit")
    cv_type = fields.Many2one('crm.type', string="Type")
    fax = fields.Char(string="Fax")

    @api.onchange('parent_id')
    def onchange_parent_id(self):
        # return values in result, as this method is used by _fields_sync()
        if not self.parent_id:
            return
        if self.parent_id:
            self.user_id = self.parent_id.user_id.id
            self.industry_id = self.parent_id.industry_id.id
        result = {}
        partner = getattr(self, '_origin', self)
        if partner.parent_id and partner.parent_id != self.parent_id:
            result['warning'] = {
                'title': _('Warning'),
                'message': _('Changing the company of a contact should only be done if it '
                             'was never correctly set. If an existing contact starts working for a new '
                             'company then a new contact should be created under that new '
                             'company. You can use the "Discard" button to abandon this change.')}
        if partner.type == 'contact' or self.type == 'contact':
            # for contacts: copy the parent address, if set (aka, at least one
            # value is set in the address: otherwise, keep the one from the
            # contact)
            address_fields = self._address_fields()
            if any(self.parent_id[key] for key in address_fields):
                def convert(value):
                    return value.id if isinstance(value, models.BaseModel) else value
                result['value'] = {key: convert(self.parent_id[key]) for key in address_fields}
        return result



class level(models.Model):
    _name = "crm.level"
    name = fields.Char(string='Level')



class company_size(models.Model):
    _name = "crm.company.size"
    name = fields.Char(string="Company Size")



class department(models.Model):
    _name = "crm.department"
    name = fields.Char(string="Department")



class businessunit(models.Model):
    _name = "crm.business.unit"
    name = fields.Char(string="Business Unit")



class vendortype(models.Model):
    _name = "crm.type"
    name = fields.Char(string="Type")

class VendorInfo(models.Model):
    _name = 'vendor.info'    
    vendor = fields.Many2one(comodel_name="res.partner", string="Vendor")
    street = fields.Char( related='vendor.street', string="Address")
    function = fields.Char( related='vendor.function', string="Job Position")
    email = fields.Char(related='vendor.email', string="Email")
    company = fields.Many2one( comodel_name="res.company", related='vendor.company_id', string="Company")
    info_vendor = fields.Many2one('crm.lead')
    
 

class crmlead(models.Model):
    _inherit = "crm.lead"
    

    pre_salesperson = fields.Many2one('res.users', string="Pre-salesperson")
    pre_industry_id = fields.Many2one('res.partner.industry', 'Industry',related="partner_id.industry_id",store=True)

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id:
            self.pre_industry_id = self.partner_id.industry_id.id
        values = self._onchange_partner_id_values(self.partner_id.id if self.partner_id else False)
        self.update(values)

    pre_job_position = fields.Char("Job Position",related="pre_salesperson.partner_id.function")
    pre_level = fields.Many2one('crm.level',related="pre_salesperson.partner_id.level", string="Level")
    pre_country_id = fields.Many2one('res.country', related="pre_salesperson.partner_id.country_id",string="Country")
    partner_name = fields.Char("Customer Name", index=True, help='The name of the future partner company that will be created while converting the lead into opportunity')
    customer_name_id = fields.Many2one('res.partner',string="Contact Name")

    # Customer Related Fields
    mobile = fields.Char('Mobile', related="customer_name_id.mobile" )
    email = fields.Char('Email', related="customer_name_id.email" )
    function = fields.Char('Job Position',related="customer_name_id.function")
    title = fields.Many2one('res.partner.title',related="customer_name_id.title")
    customer_level = fields.Many2one('crm.level',store=True, string="Level")
    # vendor1 = fields.Many2one(comodel_name="res.partner")
    # vendor2 = fields.Many2one(comodel_name="res.partner")
    
    vendor_info = fields.One2many('vendor.info','info_vendor',string="")
       
                         
    @api.onchange('customer_name_id')
    def customer_name_change(self):
        if self.customer_name_id and self.customer_name_id.level:
            self.customer_level = self.customer_name_id.level.id

    @api.onchange('partner_id')
    def partner_id_change(self):
        partners = []
        project = self.env['res.partner'].search([('parent_id', '=', self.partner_id.id)])
        if project:
            for sub in project:
                partners.append(sub.id)
            return {'domain': {
                'customer_name_id':[('id','in',partners)], 
                }}
        else:
            return {'domain': {
                'customer_name_id':[('id','in',partners)], 
                }}

    # @api.onchange('address')
    # def onchange_address(self):
    #     vendor_add = self.env['res.partner'].search([('parent_id', '=', self.partner_id.id)])
        # for record in self:
        #     return {'domain':{'partner_id':[(self.partner_type, '=',True)]}}
        