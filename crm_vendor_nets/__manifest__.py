{
    'name' : 'CRM Nets-International',
    'author': 'ERISP TEAM',
    'description': """
    Extended CRM views for Nets-international.
    """,
    'depends' : [
        
        'crm',
        'crm_extension',
        
    ],
    'data' :[
       
        'views/crm.xml',
        
    ],
    'version': '1.6.2.0',
}