from flectra import models, fields, api, _
import logging
class crm_inherited(models.Model):
    _inherit = "res.partner"
    
    
class Crmlead(models.Model):
    _inherit = 'crm.lead'    
    vendor1 = fields.Many2one(comodel_name="res.partner", string="Vendor 01")
    vendor2 = fields.Many2one(comodel_name="res.partner", string="Vendor 02")
    invoiced = fields.Float(string = "Invoiced", store=True)
    expensed = fields.Float(string = "Expenditure", store=True)
    profit = fields.Float(string = "Profit", compute='_computeVar')
    profit_result=fields.Float(string="Profit",store=True)

    # @api.onchange('invoiced', 'expensed')
    def _computeVar(self):
        for record in self:
            record.profit = record.invoiced - record.expensed
            
    @api.model 
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        res = super(Crmlead, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)
        if 'profit' in fields:
            for line in res:
                if '__domain' in line:
                    lines = self.search(line['__domain'])
                    profit_result = 0.0
                    for record in lines:
                        profit_result += record.profit
                    line['profit'] = profit_result
        return res
    